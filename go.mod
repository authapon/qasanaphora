module gitlab.com/authapon/qasanaphora

go 1.16

require (
	github.com/mattn/go-sqlite3 v1.14.6 // indirect
	gitlab.com/authapon/moosqlite v1.0.0
	gitlab.com/authapon/qascrflib v1.0.0
	gitlab.com/authapon/qassemantic v1.0.0
)
