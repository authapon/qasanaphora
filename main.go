package qasanaphora

import (
	"errors"
	"fmt"
	moosql "gitlab.com/authapon/moosqlite"
	crflib "gitlab.com/authapon/qascrflib"
	semantic "gitlab.com/authapon/qassemantic"
	"math"
	"os"
	"strconv"
	"strings"
)

const (
	LineCap = 10
)

type (
	Entity struct {
		Line        int
		Type        string
		Word        string
		POS         string
		Pat         string
		Ref         int
		Syntac      string
		Part        string
		PreWord     string
		PrePOS      string
		PreWordPat  string
		PostWord    string
		PostPOS     string
		PostWordPat string
		VerbWord    string
		VerbPOS     string
		VerbPat     string
		NewPara     bool
		NewPara2    bool
	}
)

var (
	semDB semantic.SemanticDB
)

func Anapretag(data string) string {
	dataQ := strings.Split(data, "\n")
	semDB, _ := semantic.GetSemDB()
	dataOut := ""
	enumber := 1

	for ix := range dataQ {
		start := true
		elip_logic := true
		dataout := crflib.CrfTrainSentence{}
		datax := crflib.GetWordTag(dataQ[ix])
		for i := range datax.Sentence {
			if start {
				if datax.Label[i] == "SUB" || datax.Label[i] == "CON" || datax.Label[i] == "PRL" || datax.Label[i] == "PRPpat" || datax.Label[i] == "TIMEpat" || datax.Label[i] == "AMTpat" {
					if datax.Label[i] == "AMTpat" {
						if i > 0 {
							if !(datax.Label[i-1] == "TIMEpat") {
								start = false
								dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
								dataout.Label = append(dataout.Label, datax.Label[i]+":Entity:"+fmt.Sprintf("%d", enumber))
								enumber++
								elip_logic = false
								continue
							}
						}
					}
					dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
					dataout.Label = append(dataout.Label, datax.Label[i])
					continue
				} else if datax.Label[i] == "VRBpat" || datax.Label[i] == "VRIpat" || datax.Label[i] == "ADJpat" {
					dataout.Sentence = append(dataout.Sentence, "@")
					dataout.Label = append(dataout.Label, "Zero:"+fmt.Sprintf("%d", enumber)+":0")
					enumber++
					elip_logic = false

					dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
					dataout.Label = append(dataout.Label, datax.Label[i])
					start = false
					continue
				}

			}
			start = false
			if datax.Label[i] == "HNpat" || datax.Label[i] == "VNNpat" {
				hn := crflib.GetWordTag(datax.Sentence[i])
				PRO := false
				for ii := range hn.Label {
					if hn.Label[ii] == "PRO" {
						PRO = true
					}
				}
				if PRO {
					dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
					dataout.Label = append(dataout.Label, datax.Label[i]+":Pro:"+fmt.Sprintf("%d", enumber)+":0")
					enumber++
					elip_logic = false
					continue
				}

				if i <= len(datax.Label)-2 {
					if datax.Label[i+1] == "DETpat" {
						det := crflib.GetWordTag(datax.Sentence[i+1])
						DET := false
						for ii := range det.Label {
							if det.Label[ii] == "DSO" {
								DET = true
							}
						}

						if DET {
							dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
							dataout.Label = append(dataout.Label, datax.Label[i]+":Nom:"+fmt.Sprintf("%d", enumber)+":0")
							enumber++
							elip_logic = false
							continue
						}
					}
				}

				if i+1 < len(datax.Sentence) {
					if datax.Label[i+1] == "PRPpat" && datax.Sentence[i+1] == "[ของ]<PRP>" {
						elip_logic = false
					}
				}
				if elip_logic {
					hnix := 0
					for ii := range hn.Sentence {
						if hn.Label[ii] != "NPP" {
							hnix = ii
							break
						}
					}
					elip_logic2 := true
					elip_logic3 := true
					if semDB.IsPartOfSomethingAllFromWord(hn.Sentence[hnix], hn.Label[hnix]) {
						if hnix+1 < len(hn.Sentence) {
							for ixx := hnix + 1; ixx < len(hn.Sentence); ixx++ {
								if !semDB.IsPartOfSomethingAllFromWord(hn.Sentence[ixx], hn.Label[ixx]) {
									elip_logic2 = false
									break
								}
							}
						}
						if elip_logic2 {
							dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
							dataout.Label = append(dataout.Label, datax.Label[i]+":Elipsis:"+fmt.Sprintf("%d", enumber)+":0")
							enumber++
							elip_logic = false
							continue
						}
					} else if semDB.IsPartOfSomethingAllFromWord(hn.Sentence[len(hn.Sentence)-1], hn.Label[len(hn.Sentence)-1]) {
						if len(hn.Sentence) > 1 {
							if hn.Label[len(hn.Sentence)-2] == "NCM" && !semDB.IsPartOfSomethingAllFromWord(hn.Sentence[len(hn.Sentence)-2], hn.Label[len(hn.Sentence)-2]) {
								elip_logic3 = false
							}
						}
						if elip_logic3 {
							dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
							dataout.Label = append(dataout.Label, datax.Label[i]+":Elipsis:"+fmt.Sprintf("%d", enumber)+":0")
							enumber++
							elip_logic = false
							continue
						}
					}
				}

				dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
				dataout.Label = append(dataout.Label, datax.Label[i]+":Entity:"+fmt.Sprintf("%d", enumber))
				enumber++
				elip_logic = false
				continue

			} else if datax.Label[i] == "DETpat" || datax.Label[i] == "AMTpat" {
				if i == 0 {
					dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
					dataout.Label = append(dataout.Label, datax.Label[i]+":Entity:"+fmt.Sprintf("%d", enumber))
					enumber++
					elip_logic = false
					continue
				} else if datax.Label[i-1] == "PRPpat" || datax.Label[i-1] == "VRBpat" || datax.Label[i-1] == "SUB" || datax.Label[i-1] == "CON" {
					dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
					dataout.Label = append(dataout.Label, datax.Label[i]+":Entity:"+fmt.Sprintf("%d", enumber))
					enumber++
					elip_logic = false
					continue
				}
			}

			dataout.Sentence = append(dataout.Sentence, datax.Sentence[i])
			dataout.Label = append(dataout.Label, datax.Label[i])

		}
		dataOut += crflib.WordTagString(dataout) + "\n"
	}

	return dataOut
}

func Anareref(data1, data2 string) string {
	dataOut := ""
	anaMap := make(map[string]string)
	datax1 := strings.Split(data1, "\n")
	datax2 := strings.Split(data2, "\n")
	for i := range datax2 {
		dataz1 := crflib.GetWordTag(datax1[i])
		dataz2 := crflib.GetWordTag(datax2[i])

		for ii := range dataz2.Label {
			if len(dataz2.Label[ii]) >= 4 && dataz2.Label[ii][:4] == "Zero" {
				content2 := strings.Split(dataz2.Label[ii], ":")
				for iii := range dataz1.Label {
					if dataz1.Sentence[iii] == dataz2.Sentence[ii] {
						content1 := strings.Split(dataz1.Label[iii], ":")
						anaMap[content2[1]] = content1[1]
					}
				}
			} else if (len(dataz2.Label[ii]) >= 5 && dataz2.Label[ii][:5] == "HNpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "VNNpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "DETpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "AMTpat") {
				content2 := strings.Split(dataz2.Label[ii], ":")
				if len(content2) > 1 {
					for iii := range dataz1.Label {
						content1 := strings.Split(dataz1.Label[iii], ":")
						if len(content1) > 1 && dataz1.Sentence[iii] == dataz2.Sentence[ii] && content1[0] == content2[0] {
							anaMap[content2[2]] = content1[2]
						}
					}
				}
			}
		}
	}

	for i := range datax2 {
		dataz1 := crflib.GetWordTag(datax1[i])
		dataz2 := crflib.GetWordTag(datax2[i])

		for ii := range dataz2.Label {
			if len(dataz2.Label[ii]) >= 4 && dataz2.Label[ii][:4] == "Zero" {
				content2 := strings.Split(dataz2.Label[ii], ":")
				if content2[2] == "0" {
					continue
				}
				for iii := range dataz1.Label {
					if dataz1.Sentence[iii] == dataz2.Sentence[ii] {
						content1 := strings.Split(dataz1.Label[iii], ":")
						dataz1.Label[iii] = content1[0] + ":" + content1[1] + ":" + anaMap[content2[2]]
						break
					}
				}
			} else if (len(dataz2.Label[ii]) >= 5 && dataz2.Label[ii][:5] == "HNpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "VNNpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "DETpat") {
				content2 := strings.Split(dataz2.Label[ii], ":")
				if len(content2) == 1 || content2[1] == "Entity" || content2[3] == "0" {
					continue
				}
				for iii := range dataz1.Label {
					content1 := strings.Split(dataz1.Label[iii], ":")
					if dataz1.Sentence[iii] == dataz2.Sentence[ii] && content1[0] == content2[0] {
						if len(content1) == 1 || content1[1] == "Entity" {
							continue
						}
						dataz1.Label[iii] = content1[0] + ":" + content1[1] + ":" + content1[2] + ":" + anaMap[content2[3]]
						break
					}
				}
			}
		}
		dataOut = dataOut + crflib.WordTagString(dataz1) + "\n"
	}

	return dataOut
}

func GetVerbPosition(wordTag crflib.CrfTrainSentence) int {
	for i := range wordTag.Label {
		if wordTag.Label[i] == "VRBpat" || wordTag.Label[i] == "VRIpat" {
			return i
		}
	}
	return -1
}

func ExtractWordTag(dataTag crflib.CrfTrainSentence, id int) (string, string, string) {
	pat := strings.Split(dataTag.Label[id], ":")
	if string(dataTag.Sentence[id][0]) == "[" {
		dataTagX := crflib.GetWordTag(dataTag.Sentence[id])
		word := ""
		pos := ""
		for i := range dataTagX.Sentence {
			switch i {
			case 0:
				word = dataTagX.Sentence[0]
				pos = dataTagX.Label[0]
			default:
				word += "_" + dataTagX.Sentence[i]
				pos += "_" + dataTagX.Label[i]
			}
		}
		return word, pos, pat[0]

	} else {
		return dataTag.Sentence[id], dataTag.Label[id], pat[0]
	}
}

func ExtractPOS(data crflib.CrfTrainSentence, id int) string {
	posx := strings.Split(data.Label[id], ":")
	return posx[0]
}

func XXXAnaWork(metaData []string, dataTag crflib.CrfTrainSentence, line int, wc int, verbPosition int, Entities map[int]Entity) (int, Entity) {
	i1, err := strconv.Atoi(metaData[2])
	if err != nil {
		return 0, Entity{}
	}
	i2, err := strconv.Atoi(metaData[3])
	if err != nil {
		return 0, Entity{}
	}

	ent := Entity{}
	ent.Line = line
	ent.Type = strings.ToLower(metaData[1])
	ent.Word, ent.POS, ent.Pat = ExtractWordTag(dataTag, wc)
	ent.Pat = metaData[0]
	if wc < verbPosition {
		ent.Syntac = "subject"
		if wc == 0 {
			ent.Part = "head"
		} else if posx := ExtractPOS(dataTag, wc-1); posx == "VNNpat" || posx == "PRPpat" {
			ent.Part = "part"
		} else {
			ent.Part = "head"
		}
	} else if verbPosition+1 == wc {
		ent.Syntac = "Dobject"
		ent.Part = "head"
	} else {
		ent.Syntac = "Iobject"
		ent.Part = "part"
	}
	ent.Ref = i2
	if wc > 0 {
		ent.PreWord, ent.PrePOS, ent.PreWordPat = ExtractWordTag(dataTag, wc-1)
	}
	if wc < len(dataTag.Sentence)-1 {
		ent.PostWord, ent.PostPOS, ent.PostWordPat = ExtractWordTag(dataTag, wc+1)
	}
	if verbPosition != -1 {
		ent.VerbWord, ent.VerbPOS, ent.VerbPat = ExtractWordTag(dataTag, verbPosition)
	}
	return i1, ent
}

func EntityAnaWork(metaData []string, dataTag crflib.CrfTrainSentence, line int, wc int, verbPosition int, Entities map[int]Entity) (int, Entity) {
	i1, err := strconv.Atoi(metaData[2])
	if err != nil {
		return 0, Entity{}
	}

	ent := Entity{}
	ent.Line = line
	ent.Type = "entity"
	ent.Word, ent.POS, ent.Pat = ExtractWordTag(dataTag, wc)
	ent.Pat = metaData[0]
	if wc < verbPosition {
		ent.Syntac = "subject"
		if wc == 0 {
			ent.Part = "head"
		} else if posx := ExtractPOS(dataTag, wc-1); posx == "VNNpat" || posx == "PRPpat" {
			ent.Part = "part"
		} else {
			ent.Part = "head"
		}
	} else if verbPosition+1 == wc {
		ent.Syntac = "Dobject"
		ent.Part = "head"
	} else {
		ent.Syntac = "Iobject"
		ent.Part = "part"
	}
	ent.Ref = 0
	if wc > 0 {
		ent.PreWord, ent.PrePOS, ent.PreWordPat = ExtractWordTag(dataTag, wc-1)
	}
	if wc < len(dataTag.Sentence)-1 {
		ent.PostWord, ent.PostPOS, ent.PostWordPat = ExtractWordTag(dataTag, wc+1)
	}
	if verbPosition != -1 {
		ent.VerbWord, ent.VerbPOS, ent.VerbPat = ExtractWordTag(dataTag, verbPosition)
	}
	return i1, ent
}

func ZeroAnaWork(metaData []string, dataTag crflib.CrfTrainSentence, line int, wc int, verbPosition int, Entities map[int]Entity) (int, Entity) {
	i1, err := strconv.Atoi(metaData[1])
	if err != nil {
		return 0, Entity{}
	}
	i2, err := strconv.Atoi(metaData[2])
	if err != nil {
		return 0, Entity{}
	}

	ent := Entity{}
	ent.Line = line
	ent.Type = "zero"
	ent.POS = "zero"
	ent.Word = "@"
	ent.Pat = "zero"
	ent.Syntac = "subject"
	ent.Ref = i2
	ent.Part = "head"
	if wc > 0 {
		ent.PreWord, ent.PrePOS, ent.PreWordPat = ExtractWordTag(dataTag, wc-1)
	}
	if wc < len(dataTag.Sentence)-1 {
		ent.PostWord, ent.PostPOS, ent.PostWordPat = ExtractWordTag(dataTag, wc+1)
	}
	if verbPosition != -1 {
		ent.VerbWord, ent.VerbPOS, ent.VerbPat = ExtractWordTag(dataTag, verbPosition)
	}

	return i1, ent
}

func CreateWordTag(data string) []crflib.CrfTrainSentence {
	dataLine := strings.Split(data, "\n")
	dataTag := make([]crflib.CrfTrainSentence, 0)
	for i := range dataLine {
		dataTag = append(dataTag, crflib.GetWordTag(dataLine[i]))
	}
	return dataTag
}

func ExtractEntities(data string) map[int]Entity {
	dataTag := CreateWordTag(data)
	Entities := make(map[int]Entity)
	for i := range dataTag {
		if len(dataTag[i].Sentence) == 0 {
			continue
		}
		if dataTag[i].Sentence[0] == "<end>" {
			continue
		}
		verbPosition := GetVerbPosition(dataTag[i])
		for ii := range dataTag[i].Label {
			metaData := strings.Split(dataTag[i].Label[ii], ":")
			xeid := 0
			if len(metaData) > 1 {
				if metaData[0] == "Zero" {
					eid, entityData := ZeroAnaWork(metaData, dataTag[i], i, ii, verbPosition, Entities)
					xeid = eid
					Entities[eid] = entityData
				} else {
					switch metaData[1] {
					case "Pro":
						eid, entityData := XXXAnaWork(metaData, dataTag[i], i, ii, verbPosition, Entities)
						xeid = eid
						Entities[eid] = entityData
					case "Entity":
						eid, entityData := EntityAnaWork(metaData, dataTag[i], i, ii, verbPosition, Entities)
						xeid = eid
						Entities[eid] = entityData
					case "Elipsis":
						eid, entityData := XXXAnaWork(metaData, dataTag[i], i, ii, verbPosition, Entities)
						xeid = eid
						Entities[eid] = entityData
					case "Nom":
						eid, entityData := XXXAnaWork(metaData, dataTag[i], i, ii, verbPosition, Entities)
						xeid = eid
						Entities[eid] = entityData
					}
				}
			}
			if i > 0 && xeid > 0 {
				if dataTag[i-1].Sentence[0] == "<end>" {
					newEntities := Entities[xeid]
					newEntities.NewPara = true
					Entities[xeid] = newEntities
				}
			}
			if i < len(dataTag)-1 && xeid > 0 {
				if dataTag[i+1].Sentence[0] == "<end>" {
					newEntities := Entities[xeid]
					newEntities.NewPara2 = true
					Entities[xeid] = newEntities
				}
			}

		}
	}
	return Entities
}

func FindChainAna(e map[int]Entity, i int) int {
	switch e[i].Type {
	case "entity", "elipsis":
		return i

	case "zero", "pro", "nom":
		if e[i].Ref != 0 {
			return FindChainAna(e, e[i].Ref)
		} else {
			return i
		}

	default:
		return i
	}
}

func HeadWordMatch(h1, h2 string) bool {
	hx1 := strings.Split(h1, "_")
	hx2 := strings.Split(h2, "_")
	if hx1[0] == hx2[0] {
		return true
	}
	return false
}

func FindCandidateAna(e map[int]Entity, i int) []int {
	candi := make([]int, 0)
	candi = append(candi, 0)
	for k, v := range e {
		if v.Line >= e[i].Line-LineCap && v.Line <= e[i].Line {
			if k == i {
				continue
			}
			candi = append(candi, k)
		}
	}
	return candi
}

func FindCandidateAnaX(e map[int]Entity, i int) []int {
	candi := make([]int, 0)
	candi = append(candi, e[i].Ref)
	for k, v := range e {
		if v.Line >= e[i].Line-LineCap && v.Line <= e[i].Line {
			switch k {
			case i, e[i].Ref:
				continue
			}
			candi = append(candi, k)
		}
	}
	return candi
}

func FindCandidateAnaX2(e map[int]Entity, i int) []int {
	candi := make([]int, 0)
	for k, v := range e {
		if v.Line >= e[i].Line-LineCap && v.Line <= e[i].Line {
			switch k {
			case i, e[i].Ref:
				continue
			}
			candi = append(candi, k)
		}
	}
	return candi
}

func GetAnaFeatureDB() (map[string]float64, error) {
	features := make(map[string]float64)
	db, err := moosql.GetSQL()
	if err != nil {
		return features, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `c`, `w` from `anafeature`;")
	rows, err := st.Query()
	if err != nil {
		return features, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		c := ""
		w := float64(1)
		rows.Scan(&c, &w)
		features[c] = w
	}
	return features, nil
}

func GetAnaFeatureDB0() (map[string]float64, error) {
	features := make(map[string]float64)
	db, err := moosql.GetSQL()
	if err != nil {
		return features, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `c`, `w` from `anafeature0`;")
	rows, err := st.Query()
	if err != nil {
		return features, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		c := ""
		w := float64(1)
		rows.Scan(&c, &w)
		features[c] = w
	}
	return features, nil
}

func GetAnaFeatureDBX() (map[string]float64, error) {
	features := make(map[string]float64)
	db, err := moosql.GetSQL()
	if err != nil {
		return features, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `c`, `w` from `anafeatureX`;")
	rows, err := st.Query()
	if err != nil {
		return features, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		c := ""
		w := float64(1)
		rows.Scan(&c, &w)
		features[c] = w
	}
	return features, nil
}

func Anatrain(data string) {
	round := 0
	semDB, _ = semantic.GetSemDB()
	dataTag := CreateWordTag(data)
	entities := ExtractEntities(data)
	features, err := GetAnaFeatureDB()
	if err != nil {
		fmt.Printf("Error -> %s\n", err)
		os.Exit(1)
	}
	for {
		errorAcc := float64(0)
		for i := range dataTag {
			if len(dataTag[i].Sentence) == 0 {
				continue
			}
			if dataTag[i].Sentence[0] == "<end>" {
				continue
			}
			for ii := range dataTag[i].Label {
				anaT, eid := ExtractAnaEid(dataTag[i].Label[ii], i)
				if anaT == "Entity" {
					continue
				}
				if eid > 0 {
					prob := FindAnaProb(anaT, eid, entities, features)
					for j5 := range features {
						ek := float64(0)
						for j6 := range prob {
							ek += prob[j6] * FeatureEval(anaT, j5, entities, eid, j6)
						}
						wadj := (FeatureEval(anaT, j5, entities, eid, entities[eid].Ref) - ek)
						features[j5] += wadj
						errorAcc += math.Abs(wadj)
					}
				}
			}
		}
		SaveFeature(features)
		round += 1
		fmt.Printf("\nRound %d Error Acc = %e\n", round, errorAcc)
	}
}

func Anatrain0(data string) {
	round := 0
	dataTag := CreateWordTag(data)
	entities := ExtractEntities(data)
	features, err := GetAnaFeatureDB0()
	if err != nil {
		fmt.Printf("Error -> %s\n", err)
		os.Exit(1)
	}
	for {
		errorAcc := float64(0)
		for i := range dataTag {
			if len(dataTag[i].Sentence) == 0 {
				continue
			}
			if dataTag[i].Sentence[0] == "<end>" {
				continue
			}
			for ii := range dataTag[i].Label {
				anaT, eid := ExtractAnaEid(dataTag[i].Label[ii], i)
				if anaT == "Entity" {
					continue
				}
				if eid > 0 {
					prob := FindAnaProb0(anaT, eid, entities, features)
					for j5 := range features {
						ek := float64(0)
						for j6 := range prob {
							ek += prob[j6] * FeatureEval0(anaT, j5, entities, eid, j6)
						}
						candi := 0
						if entities[eid].Ref != 0 {
							candi = 1
						}
						wadj := (FeatureEval0(anaT, j5, entities, eid, candi) - ek)
						features[j5] += wadj
						errorAcc += math.Abs(wadj)
					}
				}
			}
		}
		SaveFeature0(features)
		round += 1
		fmt.Printf("\nFeature0 Round %d Error Acc = %e\n", round, errorAcc)
	}
}

func AnatrainX(data string) {
	round := 0
	semDB, _ = semantic.GetSemDB()
	dataTag := CreateWordTag(data)
	entities := ExtractEntities(data)
	features, err := GetAnaFeatureDBX()
	if err != nil {
		fmt.Printf("Error -> %s\n", err)
		os.Exit(1)
	}
	for {
		errorAcc := float64(0)
		for i := range dataTag {
			if len(dataTag[i].Sentence) == 0 {
				continue
			}
			if dataTag[i].Sentence[0] == "<end>" {
				continue
			}
			for ii := range dataTag[i].Label {
				anaT, eid := ExtractAnaEid(dataTag[i].Label[ii], i)
				if anaT == "Entity" {
					continue
				}
				if eid > 0 {
					if entities[eid].Ref == 0 {
						continue
					}
					prob := FindAnaProbX(anaT, eid, entities, features)
					for j5 := range features {
						ek := float64(0)
						for j6 := range prob {
							ek += prob[j6] * FeatureEvalX(anaT, j5, entities, eid, j6)
						}
						wadj := (FeatureEvalX(anaT, j5, entities, eid, entities[eid].Ref) - ek)
						features[j5] += wadj
						errorAcc += math.Abs(wadj)
					}
				}
			}
		}
		SaveFeatureX(features)
		round += 1
		fmt.Printf("\nFeatureX Round %d Error Acc = %e\n", round, errorAcc)
	}
}

func ExtractAnaEid(label string, i int) (string, int) {
	anaT := ""
	eid := 0
	metaData := strings.Split(label, ":")
	if len(metaData) > 1 {
		if metaData[0] == "Zero" {
			anaT = metaData[0]
			eidx, err := strconv.Atoi(metaData[1])
			if err != nil {
				fmt.Printf("Error at Line -> %d\n", i)
				os.Exit(1)
			}
			eid = eidx
		} else {
			anaT = metaData[1]
			eidx, err := strconv.Atoi(metaData[2])
			if err != nil {
				fmt.Printf("Error at Line -> %d\n", i)
				os.Exit(1)
			}
			eid = eidx
		}
		po := fmt.Sprintf("Work on eid = %d", eid)
		fmt.Printf("%s", po)
		bspace := ""
		for _ = range po {
			bspace += "\b"
		}
		fmt.Printf("%s", bspace)
	}
	return anaT, eid
}
func FindAnaProb(anaT string, eid int, entities map[int]Entity, features map[string]float64) map[int]float64 {
	e := FindCandidateAna(entities, eid)
	score := make(map[int]float64)
	prob := make(map[int]float64)

	for j := range e {
		score[e[j]] = 0
	}
	for jj := range e {
		for featJ := range features {
			score[e[jj]] += features[featJ] * FeatureEval(anaT, featJ, entities, eid, e[jj])
		}
		score[e[jj]] = math.Exp(score[e[jj]])
	}
	TotalScore := float64(0)
	for j3 := range e {
		TotalScore += score[e[j3]]
	}
	for j4 := range e {
		prob[e[j4]] = score[e[j4]] / TotalScore
	}
	return prob
}

func FindAnaProb0(anaT string, eid int, entities map[int]Entity, features map[string]float64) map[int]float64 {
	e := make([]int, 0)
	e = append(e, 0)
	e = append(e, 1)
	score := make(map[int]float64)
	prob := make(map[int]float64)

	for j := range e {
		score[e[j]] = 0
	}
	for jj := range e {
		for featJ := range features {
			score[e[jj]] += features[featJ] * FeatureEval0(anaT, featJ, entities, eid, e[jj])
		}
		score[e[jj]] = math.Exp(score[e[jj]])
	}
	TotalScore := float64(0)
	for j3 := range e {
		TotalScore += score[e[j3]]
	}
	for j4 := range e {
		prob[e[j4]] = score[e[j4]] / TotalScore
	}
	return prob
}

func FindAnaProbX(anaT string, eid int, entities map[int]Entity, features map[string]float64) map[int]float64 {
	e := FindCandidateAnaX(entities, eid)
	score := make(map[int]float64)
	prob := make(map[int]float64)

	for j := range e {
		score[e[j]] = 0
	}
	for jj := range e {
		for featJ := range features {
			score[e[jj]] += features[featJ] * FeatureEvalX(anaT, featJ, entities, eid, e[jj])
		}
		score[e[jj]] = math.Exp(score[e[jj]])
	}
	TotalScore := float64(0)
	for j3 := range e {
		TotalScore += score[e[j3]]
	}
	for j4 := range e {
		prob[e[j4]] = score[e[j4]] / TotalScore
	}
	return prob
}

func FindAnaProbX2(anaT string, eid int, entities map[int]Entity, features map[string]float64) map[int]float64 {
	e := FindCandidateAnaX2(entities, eid)
	score := make(map[int]float64)
	prob := make(map[int]float64)

	for j := range e {
		score[e[j]] = 0
	}
	for jj := range e {
		for featJ := range features {
			score[e[jj]] += features[featJ] * FeatureEvalX(anaT, featJ, entities, eid, e[jj])
		}
		score[e[jj]] = math.Exp(score[e[jj]])
	}
	TotalScore := float64(0)
	for j3 := range e {
		TotalScore += score[e[j3]]
	}
	for j4 := range e {
		prob[e[j4]] = score[e[j4]] / TotalScore
	}
	return prob
}

func SaveFeature(f map[string]float64) {
	db, err := moosql.GetSQL()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	for k, v := range f {
		st, _ := con.Prepare("update `anafeature` set `w`=? where `c`=?;")
		_, err := st.Exec(v, k)
		if err != nil {
			fmt.Printf("Error to save feature!!!\n")
		}

	}
	return
}

func SaveFeature0(f map[string]float64) {
	db, err := moosql.GetSQL()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	for k, v := range f {
		st, _ := con.Prepare("update `anafeature0` set `w`=? where `c`=?;")
		_, err := st.Exec(v, k)
		if err != nil {
			fmt.Printf("Error to save feature!!!\n")
		}

	}
	return
}

func SaveFeatureX(f map[string]float64) {
	db, err := moosql.GetSQL()
	if err != nil {
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	for k, v := range f {
		st, _ := con.Prepare("update `anafeatureX` set `w`=? where `c`=?;")
		_, err := st.Exec(v, k)
		if err != nil {
			fmt.Printf("Error to save feature!!!\n")
		}

	}
	return
}

func AnaResolve(data string) string {
	result := ""
	dataTag := CreateWordTag(data)
	entities := ExtractEntities(data)
	anaResolve := make(map[int]int)
	features, err := GetAnaFeatureDB()
	if err != nil {
		fmt.Printf("Error -> %s\n", err)
		os.Exit(1)
	}
	for i := range dataTag {
		if len(dataTag[i].Sentence) == 0 {
			continue
		}
		if dataTag[i].Sentence[0] == "<end>" {
			continue
		}
		for ii := range dataTag[i].Label {
			anaT, eid := ExtractAnaEid(dataTag[i].Label[ii], i)
			if anaT == "Entity" {
				continue
			}
			if eid > 0 {
				prob := FindAnaProb(anaT, eid, entities, features)
				maxProb := float64(0)
				maxEid := 0
				for i3 := range prob {
					if prob[i3] > maxProb {
						maxProb = prob[i3]
						maxEid = i3
					} else if prob[i3] == maxProb {
						if maxEid == 0 {
							continue
						}
					}
				}
				anaResolve[eid] = maxEid
			}
		}
	}

	for i2 := range dataTag {
		if len(dataTag[i2].Sentence) == 0 {
			result += "\n"
			continue
		}
		if dataTag[i2].Sentence[0] == "<end>" {
			result += "<end>\n"
			continue
		}
		for ii := range dataTag[i2].Label {
			anaT, eid := ExtractAnaEid(dataTag[i2].Label[ii], i2)
			if anaT == "Entity" {
				continue
			}
			if eid > 0 {
				element := strings.Split(dataTag[i2].Label[ii], ":")
				switch anaT {
				case "Zero":
					labelF := element[0] + ":" + element[1] + ":" + fmt.Sprintf("%d", anaResolve[eid])
					dataTag[i2].Label[ii] = labelF
				case "Pro", "Elipsis", "Nom":
					labelF := element[0] + ":" + element[1] + ":" + element[2] + ":" + fmt.Sprintf("%d", anaResolve[eid])
					dataTag[i2].Label[ii] = labelF
				}
			}
		}
		result += crflib.WordTagString(dataTag[i2]) + "\n"
	}
	return result
}

func AnaResolve0(data string) string {
	result := ""
	dataTag := CreateWordTag(data)
	entities := ExtractEntities(data)
	anaResolve := make(map[int]int)
	features, err := GetAnaFeatureDB0()
	if err != nil {
		fmt.Printf("Error -> %s\n", err)
		os.Exit(1)
	}
	for i := range dataTag {
		if len(dataTag[i].Sentence) == 0 {
			continue
		}
		if dataTag[i].Sentence[0] == "<end>" {
			continue
		}
		for ii := range dataTag[i].Label {
			anaT, eid := ExtractAnaEid(dataTag[i].Label[ii], i)
			if anaT == "Entity" {
				continue
			}
			if eid > 0 {
				prob := FindAnaProb0(anaT, eid, entities, features)
				maxProb := float64(0)
				maxEid := 0
				for i3 := range prob {
					if prob[i3] > maxProb {
						maxProb = prob[i3]
						maxEid = i3
					} else if prob[i3] == maxProb {
						if maxEid == 0 {
							continue
						}
					}
				}
				anaResolve[eid] = maxEid
			}
		}
	}

	for i2 := range dataTag {
		if len(dataTag[i2].Sentence) == 0 {
			result += "\n"
			continue
		}
		if dataTag[i2].Sentence[0] == "<end>" {
			result += "<end>\n"
			continue
		}
		for ii := range dataTag[i2].Label {
			anaT, eid := ExtractAnaEid(dataTag[i2].Label[ii], i2)
			if anaT == "Entity" {
				continue
			}
			if eid > 0 {
				element := strings.Split(dataTag[i2].Label[ii], ":")
				switch anaT {
				case "Zero":
					labelF := element[0] + ":" + element[1] + ":" + fmt.Sprintf("%d", anaResolve[eid])
					dataTag[i2].Label[ii] = labelF
				case "Pro", "Elipsis", "Nom":
					labelF := element[0] + ":" + element[1] + ":" + element[2] + ":" + fmt.Sprintf("%d", anaResolve[eid])
					dataTag[i2].Label[ii] = labelF
				}
			}
		}
		result += crflib.WordTagString(dataTag[i2]) + "\n"
	}
	return result
}

func AnaResolveX(data string) string {
	result := ""
	dataTag := CreateWordTag(data)
	entities := ExtractEntities(data)
	semDB, _ = semantic.GetSemDB()
	anaResolve := make(map[int]int)
	features, err := GetAnaFeatureDBX()
	if err != nil {
		fmt.Printf("Error -> %s\n", err)
		os.Exit(1)
	}
	for i := range dataTag {
		if len(dataTag[i].Sentence) == 0 {
			continue
		}
		if dataTag[i].Sentence[0] == "<end>" {
			continue
		}
		for ii := range dataTag[i].Label {
			anaT, eid := ExtractAnaEid(dataTag[i].Label[ii], i)
			if anaT == "Entity" {
				continue
			}
			if eid > 0 {
				if entities[eid].Ref == 0 {
					continue
				}
				prob := FindAnaProbX2(anaT, eid, entities, features)
				maxProb := float64(0)
				maxEid := 0
				for i3 := range prob {
					if prob[i3] > maxProb {
						maxProb = prob[i3]
						maxEid = i3
					} else if prob[i3] == maxProb {
						if maxEid == 0 {
							continue
						}
					}
				}
				anaResolve[eid] = maxEid
			}
		}
	}

	for i2 := range dataTag {
		if len(dataTag[i2].Sentence) == 0 {
			result += "\n"
			continue
		}
		if dataTag[i2].Sentence[0] == "<end>" {
			result += "<end>\n"
			continue
		}
		for ii := range dataTag[i2].Label {
			anaT, eid := ExtractAnaEid(dataTag[i2].Label[ii], i2)
			if anaT == "Entity" {
				continue
			}
			if eid > 0 {
				element := strings.Split(dataTag[i2].Label[ii], ":")
				switch anaT {
				case "Zero":
					labelF := element[0] + ":" + element[1] + ":" + fmt.Sprintf("%d", anaResolve[eid])
					dataTag[i2].Label[ii] = labelF
				case "Pro", "Elipsis", "Nom":
					labelF := element[0] + ":" + element[1] + ":" + element[2] + ":" + fmt.Sprintf("%d", anaResolve[eid])
					dataTag[i2].Label[ii] = labelF
				}
			}
		}
		result += crflib.WordTagString(dataTag[i2]) + "\n"
	}
	return result
}

func AnaEval(data1, data2 string) {
	e1 := ExtractEntities(data1)
	e2 := ExtractEntities(data2)
	anaMap := make(map[string]string)
	anaMap2 := make(map[int]int)
	datax1 := strings.Split(data1, "\n")
	datax2 := strings.Split(data2, "\n")
	for i := range datax2 {
		dataz1 := crflib.GetWordTag(datax1[i])
		dataz2 := crflib.GetWordTag(datax2[i])

		for ii := range dataz2.Label {
			if len(dataz2.Label[ii]) >= 4 && dataz2.Label[ii][:4] == "Zero" {
				content2 := strings.Split(dataz2.Label[ii], ":")
				for iii := range dataz1.Label {
					if dataz1.Sentence[iii] == dataz2.Sentence[ii] {
						content1 := strings.Split(dataz1.Label[iii], ":")
						anaMap[content2[1]] = content1[1]
					}
				}
			} else if (len(dataz2.Label[ii]) >= 5 && dataz2.Label[ii][:5] == "HNpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "VNNpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "DETpat") || (len(dataz2.Label[ii]) >= 6 && dataz2.Label[ii][:6] == "AMTpat") {
				content2 := strings.Split(dataz2.Label[ii], ":")
				if len(content2) > 1 {
					for iii := range dataz1.Label {
						content1 := strings.Split(dataz1.Label[iii], ":")
						if len(content1) > 1 && dataz1.Sentence[iii] == dataz2.Sentence[ii] && content1[0] == content2[0] {
							anaMap[content2[2]] = content1[2]
						}
					}
				}
			}
		}
	}

	for k, v := range anaMap {
		kk, _ := strconv.Atoi(k)
		vv, _ := strconv.Atoi(v)
		anaMap2[kk] = vv
	}
	entity := 0
	zero := 0
	nom := 0
	elipsis := 0
	pro := 0

	zero0 := 0
	nom0 := 0
	elipsis0 := 0
	pro0 := 0

	zeroX := 0
	nomX := 0
	elipsisX := 0
	proX := 0

	zero00 := 0
	nom00 := 0
	elipsis00 := 0
	pro00 := 0

	zeroXX := 0
	nomXX := 0
	elipsisXX := 0
	proXX := 0

	zeroXX0 := 0
	nomXX0 := 0
	elipsisXX0 := 0
	proXX0 := 0

	for i := range e2 {
		if e2[i].Ref == 0 {
			switch e2[i].Type {
			case "entity":
				entity++
			case "zero":
				zero++
				zero0++
				if e1[anaMap2[i]].Ref == 0 {
					zero00++
				}
			case "nom":
				nom++
				nom0++
				if e1[anaMap2[i]].Ref == 0 {
					nom00++
				}
			case "elipsis":
				elipsis++
				elipsis0++
				if e1[anaMap2[i]].Ref == 0 {
					elipsis00++
				}
			case "pro":
				pro++
				pro0++
				if e1[anaMap2[i]].Ref == 0 {
					pro00++
				}
			}
		} else {
			switch e2[i].Type {
			case "entity":
				entity++
			case "zero":
				zero++
				zeroX++
				if e1[anaMap2[i]].Ref == anaMap2[e2[i].Ref] {
					zeroXX++
				}
				if e1[anaMap2[i]].Ref == 0 {
					zeroXX0++
				}
			case "nom":
				nom++
				nomX++
				if e1[anaMap2[i]].Ref == anaMap2[e2[i].Ref] {
					nomXX++
				}
				if e1[anaMap2[i]].Ref == 0 {
					nomXX0++
				}
			case "elipsis":
				elipsis++
				elipsisX++
				if e1[anaMap2[i]].Ref == anaMap2[e2[i].Ref] {
					elipsisXX++
				}
				if e1[anaMap2[i]].Ref == 0 {
					elipsisXX0++
				}
			case "pro":
				pro++
				proX++
				if e1[anaMap2[i]].Ref == anaMap2[e2[i].Ref] {
					proXX++
				}
				if e1[anaMap2[i]].Ref == 0 {
					proXX0++
				}
			}
		}
	}

	fmt.Printf("Total Entity   = %v\n\n", entity)
	fmt.Printf("Total Zero     = %v\n", zero)
	fmt.Printf("Total Nom      = %v\n", nom)
	fmt.Printf("Total Elipsis  = %v\n", elipsis)
	fmt.Printf("Total Pro      = %v\n\n", pro)

	fmt.Printf("Total Zero0    = %v -> %v\n", zero0, zero00)
	fmt.Printf("Total Nom0     = %v -> %v\n", nom0, nom00)
	fmt.Printf("Total Elipsis0 = %v -> %v\n", elipsis0, elipsis00)
	fmt.Printf("Total Pro0     = %v -> %v\n\n", pro0, pro00)

	fmt.Printf("Total ZeroX    = %v -> %v -- (%v)\n", zeroX, zeroXX, zeroXX0)
	fmt.Printf("Total NomX     = %v -> %v -- (%v)\n", nomX, nomXX, nomXX0)
	fmt.Printf("Total ElipsisX = %v -> %v -- (%v)\n", elipsisX, elipsisXX, elipsisXX0)
	fmt.Printf("Total ProX     = %v -> %v -- (%v)\n\n", proX, proXX, proXX0)
}
