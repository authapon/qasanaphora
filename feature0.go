package qasanaphora

import (
	"fmt"
	moosql "gitlab.com/authapon/moosqlite"
	"strings"
)

func Anafeature0(data string) {
	Entities := ExtractEntities(data)
	anaFeats := make(map[string]bool)
	for ii := range Entities {
		switch Entities[ii].Type {
		case "zero":
			switch Entities[ii].Ref {
			case 0:
				AFzero0Y(Entities, ii, anaFeats)
			default:
				AFzero0N(Entities, ii, anaFeats)
			}
		case "pro":
			switch Entities[ii].Ref {
			case 0:
				AFpro0Y(Entities, ii, anaFeats)
			default:
				AFpro0N(Entities, ii, anaFeats)
			}
		case "elipsis":
			switch Entities[ii].Ref {
			case 0:
				AFelip0Y(Entities, ii, anaFeats)
			default:
				AFelip0N(Entities, ii, anaFeats)
			}
		case "nom":
			switch Entities[ii].Ref {
			case 0:
				AFnom0Y(Entities, ii, anaFeats)
			default:
				AFnom0N(Entities, ii, anaFeats)
			}
		default:
		}
	}

	db, err := moosql.GetSQL()
	if err != nil {
		fmt.Printf("DB connection fail!")
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	for k, _ := range anaFeats {
		st, _ := con.Prepare("insert into `anafeature0` (`c`) values (?);")
		fmt.Printf("%s\n", k)
		st.Exec(k)
	}
}

func AFzero0Y(e map[int]Entity, i int, af map[string]bool) {
	af["zero0Y1:"+e[i].VerbWord] = true
	af["zero0Y2:"+e[i].VerbPOS] = true
	af["zero0Y3:"+e[i].VerbPat] = true
	af["zero0Y4:"+e[i].PreWord] = true
	af["zero0Y5:"+e[i].PrePOS] = true
	af["zero0Y6:"+e[i].PreWordPat] = true
	af["zero0Y7:"+e[i].PostWord] = true
	af["zero0Y8:"+e[i].PostPOS] = true
	af["zero0Y9:"+e[i].PostWordPat] = true
	af["zero0Y10:"+e[i].Syntac] = true
	af["zero0Y11:"+e[i].Part] = true
	af["zero0Y12:"+e[i].Word] = true
	af["zero0Y13:"+e[i].POS] = true
	af["zero0Y14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["zero0Y15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["zero0Y16:NewPara2"] = true
	}
}

func AFzero0N(e map[int]Entity, i int, af map[string]bool) {
	af["zero0N1:"+e[i].VerbWord] = true
	af["zero0N2:"+e[i].VerbPOS] = true
	af["zero0N3:"+e[i].VerbPat] = true
	af["zero0N4:"+e[i].PreWord] = true
	af["zero0N5:"+e[i].PrePOS] = true
	af["zero0N6:"+e[i].PreWordPat] = true
	af["zero0N7:"+e[i].PostWord] = true
	af["zero0N8:"+e[i].PostPOS] = true
	af["zero0N9:"+e[i].PostWordPat] = true
	af["zero0N10:"+e[i].Syntac] = true
	af["zero0N11:"+e[i].Part] = true
	af["zero0N12:"+e[i].Word] = true
	af["zero0N13:"+e[i].POS] = true
	af["zero0N14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["zero0N15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["zero0N16:NewPara2"] = true
	}
}

func AFpro0Y(e map[int]Entity, i int, af map[string]bool) {
	af["pro0Y1:"+e[i].VerbWord] = true
	af["pro0Y2:"+e[i].VerbPOS] = true
	af["pro0Y3:"+e[i].VerbPat] = true
	af["pro0Y4:"+e[i].PreWord] = true
	af["pro0Y5:"+e[i].PrePOS] = true
	af["pro0Y6:"+e[i].PreWordPat] = true
	af["pro0Y7:"+e[i].PostWord] = true
	af["pro0Y8:"+e[i].PostPOS] = true
	af["pro0Y9:"+e[i].PostWordPat] = true
	af["pro0Y10:"+e[i].Syntac] = true
	af["pro0Y11:"+e[i].Part] = true
	af["pro0Y12:"+e[i].Word] = true
	af["pro0Y13:"+e[i].POS] = true
	af["pro0Y14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["pro0Y15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["pro0Y16:NewPara2"] = true
	}
}

func AFpro0N(e map[int]Entity, i int, af map[string]bool) {
	af["pro0N1:"+e[i].VerbWord] = true
	af["pro0N2:"+e[i].VerbPOS] = true
	af["pro0N3:"+e[i].VerbPat] = true
	af["pro0N4:"+e[i].PreWord] = true
	af["pro0N5:"+e[i].PrePOS] = true
	af["pro0N6:"+e[i].PreWordPat] = true
	af["pro0N7:"+e[i].PostWord] = true
	af["pro0N8:"+e[i].PostPOS] = true
	af["pro0N9:"+e[i].PostWordPat] = true
	af["pro0N10:"+e[i].Syntac] = true
	af["pro0N11:"+e[i].Part] = true
	af["pro0N12:"+e[i].Word] = true
	af["pro0N13:"+e[i].POS] = true
	af["pro0N14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["pro0N15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["pro0N16:NewPara2"] = true
	}
}

func AFelip0Y(e map[int]Entity, i int, af map[string]bool) {
	af["elip0Y1:"+e[i].VerbWord] = true
	af["elip0Y2:"+e[i].VerbPOS] = true
	af["elip0Y3:"+e[i].VerbPat] = true
	af["elip0Y4:"+e[i].PreWord] = true
	af["elip0Y5:"+e[i].PrePOS] = true
	af["elip0Y6:"+e[i].PreWordPat] = true
	af["elip0Y7:"+e[i].PostWord] = true
	af["elip0Y8:"+e[i].PostPOS] = true
	af["elip0Y9:"+e[i].PostWordPat] = true
	af["elip0Y10:"+e[i].Syntac] = true
	af["elip0Y11:"+e[i].Part] = true
	af["elip0Y12:"+e[i].Word] = true
	af["elip0Y13:"+e[i].POS] = true
	af["elip0Y14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["elip0Y15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["elip0Y16:NewPara2"] = true
	}
}

func AFelip0N(e map[int]Entity, i int, af map[string]bool) {
	af["elip0N1:"+e[i].VerbWord] = true
	af["elip0N2:"+e[i].VerbPOS] = true
	af["elip0N3:"+e[i].VerbPat] = true
	af["elip0N4:"+e[i].PreWord] = true
	af["elip0N5:"+e[i].PrePOS] = true
	af["elip0N6:"+e[i].PreWordPat] = true
	af["elip0N7:"+e[i].PostWord] = true
	af["elip0N8:"+e[i].PostPOS] = true
	af["elip0N9:"+e[i].PostWordPat] = true
	af["elip0N10:"+e[i].Syntac] = true
	af["elip0N11:"+e[i].Part] = true
	af["elip0N12:"+e[i].Word] = true
	af["elip0N13:"+e[i].POS] = true
	af["elip0N14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["elip0N15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["elip0N16:NewPara2"] = true
	}
}

func AFnom0Y(e map[int]Entity, i int, af map[string]bool) {
	af["nom0Y1:"+e[i].VerbWord] = true
	af["nom0Y2:"+e[i].VerbPOS] = true
	af["nom0Y3:"+e[i].VerbPat] = true
	af["nom0Y4:"+e[i].PreWord] = true
	af["nom0Y5:"+e[i].PrePOS] = true
	af["nom0Y6:"+e[i].PreWordPat] = true
	af["nom0Y7:"+e[i].PostWord] = true
	af["nom0Y8:"+e[i].PostPOS] = true
	af["nom0Y9:"+e[i].PostWordPat] = true
	af["nom0Y10:"+e[i].Syntac] = true
	af["nom0Y11:"+e[i].Part] = true
	af["nom0Y12:"+e[i].Word] = true
	af["nom0Y13:"+e[i].POS] = true
	af["nom0Y14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["nom0Y15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["nom0Y16:NewPara2"] = true
	}
}

func AFnom0N(e map[int]Entity, i int, af map[string]bool) {
	af["nom0N1:"+e[i].VerbWord] = true
	af["nom0N2:"+e[i].VerbPOS] = true
	af["nom0N3:"+e[i].VerbPat] = true
	af["nom0N4:"+e[i].PreWord] = true
	af["nom0N5:"+e[i].PrePOS] = true
	af["nom0N6:"+e[i].PreWordPat] = true
	af["nom0N7:"+e[i].PostWord] = true
	af["nom0N8:"+e[i].PostPOS] = true
	af["nom0N9:"+e[i].PostWordPat] = true
	af["nom0N10:"+e[i].Syntac] = true
	af["nom0N11:"+e[i].Part] = true
	af["nom0N12:"+e[i].Word] = true
	af["nom0N13:"+e[i].POS] = true
	af["nom0N14:"+e[i].Pat] = true
	if e[i].NewPara {
		af["nom0N15:NewPara"] = true
	}
	if e[i].NewPara2 {
		af["nom0N16:NewPara2"] = true
	}
}

func FeatureEval0(anaType string, feature string, ent map[int]Entity, eid int, candi int) float64 {
	f := strings.Split(feature, ":")

	switch {
	case anaType == "Zero" && f[0] == "zero0Y1" && f[1] == ent[eid].VerbWord && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y2" && f[1] == ent[eid].VerbPOS && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y3" && f[1] == ent[eid].VerbPat && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y4" && f[1] == ent[eid].PreWord && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y5" && f[1] == ent[eid].PrePOS && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y6" && f[1] == ent[eid].PreWordPat && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y7" && f[1] == ent[eid].PostWord && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y8" && f[1] == ent[eid].PostPOS && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y9" && f[1] == ent[eid].PostWordPat && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y10" && f[1] == ent[eid].Syntac && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y11" && f[1] == ent[eid].Part && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y12" && f[1] == ent[eid].Word && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y13" && f[1] == ent[eid].POS && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y14" && f[1] == ent[eid].Pat && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y15" && ent[eid].NewPara && candi == 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0Y16" && ent[eid].NewPara2 && candi == 0:
		return 1

	case anaType == "Zero" && f[0] == "zero0N1" && f[1] == ent[eid].VerbWord && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N2" && f[1] == ent[eid].VerbPOS && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N3" && f[1] == ent[eid].VerbPat && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N4" && f[1] == ent[eid].PreWord && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N5" && f[1] == ent[eid].PrePOS && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N6" && f[1] == ent[eid].PreWordPat && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N7" && f[1] == ent[eid].PostWord && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N8" && f[1] == ent[eid].PostPOS && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N9" && f[1] == ent[eid].PostWordPat && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N10" && f[1] == ent[eid].Syntac && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N11" && f[1] == ent[eid].Part && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N12" && f[1] == ent[eid].Word && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N13" && f[1] == ent[eid].POS && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N14" && f[1] == ent[eid].Pat && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N15" && ent[eid].NewPara && candi != 0:
		return 1
	case anaType == "Zero" && f[0] == "zero0N16" && ent[eid].NewPara2 && candi != 0:
		return 1

	case anaType == "Pro" && f[0] == "pro0Y1" && f[1] == ent[eid].VerbWord && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y2" && f[1] == ent[eid].VerbPOS && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y3" && f[1] == ent[eid].VerbPat && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y4" && f[1] == ent[eid].PreWord && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y5" && f[1] == ent[eid].PrePOS && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y6" && f[1] == ent[eid].PreWordPat && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y7" && f[1] == ent[eid].PostWord && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y8" && f[1] == ent[eid].PostPOS && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y9" && f[1] == ent[eid].PostWordPat && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y10" && f[1] == ent[eid].Syntac && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y11" && f[1] == ent[eid].Part && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y12" && f[1] == ent[eid].Word && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y13" && f[1] == ent[eid].POS && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y14" && f[1] == ent[eid].Pat && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y15" && ent[eid].NewPara && candi == 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0Y16" && ent[eid].NewPara2 && candi == 0:
		return 1

	case anaType == "Pro" && f[0] == "pro0N1" && f[1] == ent[eid].VerbWord && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N2" && f[1] == ent[eid].VerbPOS && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N3" && f[1] == ent[eid].VerbPat && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N4" && f[1] == ent[eid].PreWord && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N5" && f[1] == ent[eid].PrePOS && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N6" && f[1] == ent[eid].PreWordPat && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N7" && f[1] == ent[eid].PostWord && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N8" && f[1] == ent[eid].PostPOS && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N9" && f[1] == ent[eid].PostWordPat && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N10" && f[1] == ent[eid].Syntac && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N11" && f[1] == ent[eid].Part && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N12" && f[1] == ent[eid].Word && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N13" && f[1] == ent[eid].POS && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N14" && f[1] == ent[eid].Pat && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N15" && ent[eid].NewPara && candi != 0:
		return 1
	case anaType == "Pro" && f[0] == "pro0N16" && ent[eid].NewPara2 && candi != 0:
		return 1

	case anaType == "Elipsis" && f[0] == "elip0Y1" && f[1] == ent[eid].VerbWord && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y2" && f[1] == ent[eid].VerbPOS && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y3" && f[1] == ent[eid].VerbPat && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y4" && f[1] == ent[eid].PreWord && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y5" && f[1] == ent[eid].PrePOS && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y6" && f[1] == ent[eid].PreWordPat && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y7" && f[1] == ent[eid].PostWord && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y8" && f[1] == ent[eid].PostPOS && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y9" && f[1] == ent[eid].PostWordPat && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y10" && f[1] == ent[eid].Syntac && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y11" && f[1] == ent[eid].Part && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y12" && f[1] == ent[eid].Word && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y13" && f[1] == ent[eid].POS && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y14" && f[1] == ent[eid].Pat && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y15" && ent[eid].NewPara && candi == 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0Y16" && ent[eid].NewPara2 && candi == 0:
		return 1

	case anaType == "Elipsis" && f[0] == "elip0N1" && f[1] == ent[eid].VerbWord && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N2" && f[1] == ent[eid].VerbPOS && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N3" && f[1] == ent[eid].VerbPat && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N4" && f[1] == ent[eid].PreWord && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N5" && f[1] == ent[eid].PrePOS && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N6" && f[1] == ent[eid].PreWordPat && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N7" && f[1] == ent[eid].PostWord && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N8" && f[1] == ent[eid].PostPOS && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N9" && f[1] == ent[eid].PostWordPat && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N10" && f[1] == ent[eid].Syntac && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N11" && f[1] == ent[eid].Part && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N12" && f[1] == ent[eid].Word && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N13" && f[1] == ent[eid].POS && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N14" && f[1] == ent[eid].Pat && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N15" && ent[eid].NewPara && candi != 0:
		return 1
	case anaType == "Elipsis" && f[0] == "elip0N16" && ent[eid].NewPara2 && candi != 0:
		return 1

	case anaType == "Nom" && f[0] == "nom0Y1" && f[1] == ent[eid].VerbWord && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y2" && f[1] == ent[eid].VerbPOS && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y3" && f[1] == ent[eid].VerbPat && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y4" && f[1] == ent[eid].PreWord && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y5" && f[1] == ent[eid].PrePOS && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y6" && f[1] == ent[eid].PreWordPat && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y7" && f[1] == ent[eid].PostWord && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y8" && f[1] == ent[eid].PostPOS && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y9" && f[1] == ent[eid].PostWordPat && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y10" && f[1] == ent[eid].Syntac && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y11" && f[1] == ent[eid].Part && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y12" && f[1] == ent[eid].Word && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y13" && f[1] == ent[eid].POS && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y14" && f[1] == ent[eid].Pat && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y15" && ent[eid].NewPara && candi == 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0Y16" && ent[eid].NewPara2 && candi == 0:
		return 1

	case anaType == "Nom" && f[0] == "nom0N1" && f[1] == ent[eid].VerbWord && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N2" && f[1] == ent[eid].VerbPOS && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N3" && f[1] == ent[eid].VerbPat && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N4" && f[1] == ent[eid].PreWord && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N5" && f[1] == ent[eid].PrePOS && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N6" && f[1] == ent[eid].PreWordPat && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N7" && f[1] == ent[eid].PostWord && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N8" && f[1] == ent[eid].PostPOS && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N9" && f[1] == ent[eid].PostWordPat && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N10" && f[1] == ent[eid].Syntac && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N11" && f[1] == ent[eid].Part && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N12" && f[1] == ent[eid].Word && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N13" && f[1] == ent[eid].POS && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N14" && f[1] == ent[eid].Pat && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N15" && ent[eid].NewPara && candi != 0:
		return 1
	case anaType == "Nom" && f[0] == "nom0N16" && ent[eid].NewPara2 && candi != 0:
		return 1
	}
	return 0
}
