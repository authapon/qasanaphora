package qasanaphora

import (
	"fmt"
	moosql "gitlab.com/authapon/moosqlite"
	semantic "gitlab.com/authapon/qassemantic"
	"strconv"
	"strings"
)

func AnafeatureX(data string) {
	Entities := ExtractEntities(data)
	anaFeats := make(map[string]bool)
	semDB, _ = semantic.GetSemDB()
	for ii := range Entities {
		switch Entities[ii].Type {
		case "zero":
			switch Entities[ii].Ref {
			case 0:
				continue
			default:
				AFzeroX(Entities, ii, anaFeats)
			}
		case "pro":
			switch Entities[ii].Ref {
			case 0:
				continue
			default:
				AFproX(Entities, ii, anaFeats)
			}
		case "elipsis":
			switch Entities[ii].Ref {
			case 0:
				continue
			default:
				AFelipX(Entities, ii, anaFeats)
			}
		case "nom":
			switch Entities[ii].Ref {
			case 0:
				continue
			default:
				AFnomX(Entities, ii, anaFeats)
			}
		default:
		}
	}

	db, err := moosql.GetSQL()
	if err != nil {
		fmt.Printf("DB connection fail!")
		return
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	for k, _ := range anaFeats {
		st, _ := con.Prepare("insert into `anafeatureX` (`c`) values (?);")
		fmt.Printf("%s\n", k)
		st.Exec(k)
	}
}

func AFzeroX(e map[int]Entity, i int, af map[string]bool) {
	distance := fmt.Sprintf("%d", e[i].Line-e[e[i].Ref].Line)

	af["zeroXA1:"+e[i].VerbWord+":"+distance] = true
	af["zeroXA2:"+e[i].VerbPOS+":"+distance] = true
	af["zeroXA3:"+e[i].VerbPat+":"+distance] = true
	af["zeroXA4:"+e[i].PreWord+":"+distance] = true
	af["zeroXA5:"+e[i].PrePOS+":"+distance] = true
	af["zeroXA6:"+e[i].PreWordPat+":"+distance] = true
	af["zeroXA7:"+e[i].PostWord+":"+distance] = true
	af["zeroXA8:"+e[i].PostPOS+":"+distance] = true
	af["zeroXA9:"+e[i].PostWordPat+":"+distance] = true
	af["zeroXA10:"+e[i].Syntac+":"+distance] = true
	af["zeroXA11:"+e[i].Part+":"+distance] = true
	af["zeroXA12:"+e[i].Word+":"+distance] = true
	af["zeroXA13:"+e[i].POS+":"+distance] = true
	af["zeroXA14:"+e[i].Pat+":"+distance] = true
	if e[i].NewPara {
		af["zeroXA15:NewPara"+":"+distance] = true
	}
	if e[i].NewPara2 {
		af["zeroXA16:NewPara2"+":"+distance] = true
	}

	af["zeroXB1:"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["zeroXB2:"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["zeroXB3:"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["zeroXB4:"+e[e[i].Ref].PreWord+":"+distance] = true
	af["zeroXB5:"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["zeroXB6:"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["zeroXB7:"+e[e[i].Ref].PostWord+":"+distance] = true
	af["zeroXB8:"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["zeroXB9:"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["zeroXB10:"+e[e[i].Ref].Syntac+":"+distance] = true
	af["zeroXB11:"+e[e[i].Ref].Part+":"+distance] = true
	ref := FindChainAna(e, e[i].Ref)
	af["zeroXB12:"+e[ref].Word+":"+distance] = true
	af["zeroXB13:"+e[ref].POS+":"+distance] = true
	af["zeroXB14:"+e[ref].Pat+":"+distance] = true

	af["zeroXC1:"+e[i].VerbWord+":"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["zeroXC2:"+e[i].VerbPOS+":"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["zeroXC3:"+e[i].VerbPat+":"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["zeroXC4:"+e[i].PreWord+":"+e[e[i].Ref].PreWord+":"+distance] = true
	af["zeroXC5:"+e[i].PrePOS+":"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["zeroXC6:"+e[i].PreWordPat+":"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["zeroXC7:"+e[i].PostWord+":"+e[e[i].Ref].PostWord+":"+distance] = true
	af["zeroXC8:"+e[i].PostPOS+":"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["zeroXC9:"+e[i].PostWordPat+":"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["zeroXC10:"+e[i].Syntac+":"+e[e[i].Ref].Syntac+":"+distance] = true
	af["zeroXC11:"+e[i].Part+":"+e[e[i].Ref].Part+":"+distance] = true
	af["zeroXC12:"+e[i].Word+":"+e[ref].Word+":"+distance] = true
	af["zeroXC13:"+e[i].POS+":"+e[ref].POS+":"+distance] = true
	af["zeroXC14:"+e[i].Pat+":"+e[ref].Pat+":"+distance] = true
}

func AFproX(e map[int]Entity, i int, af map[string]bool) {
	distance := fmt.Sprintf("%d", e[i].Line-e[e[i].Ref].Line)

	af["proXA1:"+e[i].VerbWord+":"+distance] = true
	af["proXA2:"+e[i].VerbPOS+":"+distance] = true
	af["proXA3:"+e[i].VerbPat+":"+distance] = true
	af["proXA4:"+e[i].PreWord+":"+distance] = true
	af["proXA5:"+e[i].PrePOS+":"+distance] = true
	af["proXA6:"+e[i].PreWordPat+":"+distance] = true
	af["proXA7:"+e[i].PostWord+":"+distance] = true
	af["proXA8:"+e[i].PostPOS+":"+distance] = true
	af["proXA9:"+e[i].PostWordPat+":"+distance] = true
	af["proXA10:"+e[i].Syntac+":"+distance] = true
	af["proXA11:"+e[i].Part+":"+distance] = true
	af["proXA12:"+e[i].Word+":"+distance] = true
	af["proXA13:"+e[i].POS+":"+distance] = true
	af["proXA14:"+e[i].Pat+":"+distance] = true
	if e[i].NewPara {
		af["proXA15:NewPara"+":"+distance] = true
	}
	if e[i].NewPara2 {
		af["proXA16:NewPara2"+":"+distance] = true
	}

	af["proXB1:"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["proXB2:"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["proXB3:"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["proXB4:"+e[e[i].Ref].PreWord+":"+distance] = true
	af["proXB5:"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["proXB6:"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["proXB7:"+e[e[i].Ref].PostWord+":"+distance] = true
	af["proXB8:"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["proXB9:"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["proXB10:"+e[e[i].Ref].Syntac+":"+distance] = true
	af["proXB11:"+e[e[i].Ref].Part+":"+distance] = true
	ref := FindChainAna(e, e[i].Ref)
	af["proXB12:"+e[ref].Word+":"+distance] = true
	af["proXB13:"+e[ref].POS+":"+distance] = true
	af["proXB14:"+e[ref].Pat+":"+distance] = true
	af["proXB15:"+e[i].Word+":"+e[ref].Word] = true
	if HeadWordMatch(e[i].Word, e[ref].Word) {
		af["proXB16:match:"+distance] = true
	}
	w1 := strings.Split(e[i].Word, "_")
	pos1 := strings.Split(e[i].POS, "_")
	w2 := strings.Split(e[ref].Word, "_")
	pos2 := strings.Split(e[ref].POS, "_")
	c1 := w1[0] + ":" + pos1[0]
	c2 := w2[0] + ":" + pos2[0]
	if semDB.ISAWord(c1, c2) {
		af["proXB17:isa:"+distance] = true
	}

	af["proXC1:"+e[i].VerbWord+":"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["proXC2:"+e[i].VerbPOS+":"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["proXC3:"+e[i].VerbPat+":"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["proXC4:"+e[i].PreWord+":"+e[e[i].Ref].PreWord+":"+distance] = true
	af["proXC5:"+e[i].PrePOS+":"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["proXC6:"+e[i].PreWordPat+":"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["proXC7:"+e[i].PostWord+":"+e[e[i].Ref].PostWord+":"+distance] = true
	af["proXC8:"+e[i].PostPOS+":"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["proXC9:"+e[i].PostWordPat+":"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["proXC10:"+e[i].Syntac+":"+e[e[i].Ref].Syntac+":"+distance] = true
	af["proXC11:"+e[i].Part+":"+e[e[i].Ref].Part+":"+distance] = true
	af["proXC12:"+e[i].Word+":"+e[ref].Word+":"+distance] = true
	af["proXC13:"+e[i].POS+":"+e[ref].POS+":"+distance] = true
	af["proXC14:"+e[i].Pat+":"+e[ref].Pat+":"+distance] = true
}

func AFelipX(e map[int]Entity, i int, af map[string]bool) {
	distance := fmt.Sprintf("%d", e[i].Line-e[e[i].Ref].Line)

	af["elipXA1:"+e[i].VerbWord+":"+distance] = true
	af["elipXA2:"+e[i].VerbPOS+":"+distance] = true
	af["elipXA3:"+e[i].VerbPat+":"+distance] = true
	af["elipXA4:"+e[i].PreWord+":"+distance] = true
	af["elipXA5:"+e[i].PrePOS+":"+distance] = true
	af["elipXA6:"+e[i].PreWordPat+":"+distance] = true
	af["elipXA7:"+e[i].PostWord+":"+distance] = true
	af["elipXA8:"+e[i].PostPOS+":"+distance] = true
	af["elipXA9:"+e[i].PostWordPat+":"+distance] = true
	af["elipXA10:"+e[i].Syntac+":"+distance] = true
	af["elipXA11:"+e[i].Part+":"+distance] = true
	af["elipXA12:"+e[i].Word+":"+distance] = true
	af["elipXA13:"+e[i].POS+":"+distance] = true
	af["elipXA14:"+e[i].Pat+":"+distance] = true
	if e[i].NewPara {
		af["elipXA15:NewPara"+":"+distance] = true
	}
	if e[i].NewPara2 {
		af["elipXA16:NewPara2"+":"+distance] = true
	}

	af["elipXB1:"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["elipXB2:"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["elipXB3:"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["elipXB4:"+e[e[i].Ref].PreWord+":"+distance] = true
	af["elipXB5:"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["elipXB6:"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["elipXB7:"+e[e[i].Ref].PostWord+":"+distance] = true
	af["elipXB8:"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["elipXB9:"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["elipXB10:"+e[e[i].Ref].Syntac+":"+distance] = true
	af["elipXB11:"+e[e[i].Ref].Part+":"+distance] = true
	ref := FindChainAna(e, e[i].Ref)
	af["elipXB12:"+e[ref].Word+":"+distance] = true
	af["elipXB13:"+e[ref].POS+":"+distance] = true
	af["elipXB14:"+e[ref].Pat+":"+distance] = true
	af["elipXB15:"+e[i].Word+":"+e[ref].Word] = true
	if HeadWordMatch(e[i].Word, e[ref].Word) {
		af["elipXB16:match:"+distance] = true
	}
	w1 := strings.Split(e[i].Word, "_")
	pos1 := strings.Split(e[i].POS, "_")
	w2 := strings.Split(e[ref].Word, "_")
	pos2 := strings.Split(e[ref].POS, "_")
	c1 := w1[0] + ":" + pos1[0]
	c2 := w2[0] + ":" + pos2[0]
	if semDB.ISAWord(c1, c2) {
		af["elipXB17:isa:"+distance] = true
	}

	af["elipXC1:"+e[i].VerbWord+":"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["elipXC2:"+e[i].VerbPOS+":"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["elipXC3:"+e[i].VerbPat+":"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["elipXC4:"+e[i].PreWord+":"+e[e[i].Ref].PreWord+":"+distance] = true
	af["elipXC5:"+e[i].PrePOS+":"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["elipXC6:"+e[i].PreWordPat+":"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["elipXC7:"+e[i].PostWord+":"+e[e[i].Ref].PostWord+":"+distance] = true
	af["elipXC8:"+e[i].PostPOS+":"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["elipXC9:"+e[i].PostWordPat+":"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["elipXC10:"+e[i].Syntac+":"+e[e[i].Ref].Syntac+":"+distance] = true
	af["elipXC11:"+e[i].Part+":"+e[e[i].Ref].Part+":"+distance] = true
	af["elipXC12:"+e[i].Word+":"+e[ref].Word+":"+distance] = true
	af["elipXC13:"+e[i].POS+":"+e[ref].POS+":"+distance] = true
	af["elipXC14:"+e[i].Pat+":"+e[ref].Pat+":"+distance] = true
}

func AFnomX(e map[int]Entity, i int, af map[string]bool) {
	distance := fmt.Sprintf("%d", e[i].Line-e[e[i].Ref].Line)

	af["nomXA1:"+e[i].VerbWord+":"+distance] = true
	af["nomXA2:"+e[i].VerbPOS+":"+distance] = true
	af["nomXA3:"+e[i].VerbPat+":"+distance] = true
	af["nomXA4:"+e[i].PreWord+":"+distance] = true
	af["nomXA5:"+e[i].PrePOS+":"+distance] = true
	af["nomXA6:"+e[i].PreWordPat+":"+distance] = true
	af["nomXA7:"+e[i].PostWord+":"+distance] = true
	af["nomXA8:"+e[i].PostPOS+":"+distance] = true
	af["nomXA9:"+e[i].PostWordPat+":"+distance] = true
	af["nomXA10:"+e[i].Syntac+":"+distance] = true
	af["nomXA11:"+e[i].Part+":"+distance] = true
	af["nomXA12:"+e[i].Word+":"+distance] = true
	af["nomXA13:"+e[i].POS+":"+distance] = true
	af["nomXA14:"+e[i].Pat+":"+distance] = true
	if e[i].NewPara {
		af["nomXA15:NewPara"+":"+distance] = true
	}
	if e[i].NewPara2 {
		af["nomXA16:NewPara2"+":"+distance] = true
	}

	af["nomXB1:"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["nomXB2:"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["nomXB3:"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["nomXB4:"+e[e[i].Ref].PreWord+":"+distance] = true
	af["nomXB5:"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["nomXB6:"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["nomXB7:"+e[e[i].Ref].PostWord+":"+distance] = true
	af["nomXB8:"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["nomXB9:"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["nomXB10:"+e[e[i].Ref].Syntac+":"+distance] = true
	af["nomXB11:"+e[e[i].Ref].Part+":"+distance] = true
	ref := FindChainAna(e, e[i].Ref)
	af["nomXB12:"+e[ref].Word+":"+distance] = true
	af["nomXB13:"+e[ref].POS+":"+distance] = true
	af["nomXB14:"+e[ref].Pat+":"+distance] = true
	af["nomXB15:"+e[i].Word+":"+e[ref].Word] = true
	if HeadWordMatch(e[i].Word, e[ref].Word) {
		af["nomXB16:match:"+distance] = true
	}
	w1 := strings.Split(e[i].Word, "_")
	pos1 := strings.Split(e[i].POS, "_")
	w2 := strings.Split(e[ref].Word, "_")
	pos2 := strings.Split(e[ref].POS, "_")
	c1 := w1[0] + ":" + pos1[0]
	c2 := w2[0] + ":" + pos2[0]
	if semDB.ISAWord(c1, c2) {
		af["nomXB17:isa:"+distance] = true
	}

	af["nomXC1:"+e[i].VerbWord+":"+e[e[i].Ref].VerbWord+":"+distance] = true
	af["nomXC2:"+e[i].VerbPOS+":"+e[e[i].Ref].VerbPOS+":"+distance] = true
	af["nomXC3:"+e[i].VerbPat+":"+e[e[i].Ref].VerbPat+":"+distance] = true
	af["nomXC4:"+e[i].PreWord+":"+e[e[i].Ref].PreWord+":"+distance] = true
	af["nomXC5:"+e[i].PrePOS+":"+e[e[i].Ref].PrePOS+":"+distance] = true
	af["nomXC6:"+e[i].PreWordPat+":"+e[e[i].Ref].PreWordPat+":"+distance] = true
	af["nomXC7:"+e[i].PostWord+":"+e[e[i].Ref].PostWord+":"+distance] = true
	af["nomXC8:"+e[i].PostPOS+":"+e[e[i].Ref].PostPOS+":"+distance] = true
	af["nomXC9:"+e[i].PostWordPat+":"+e[e[i].Ref].PostWordPat+":"+distance] = true
	af["nomXC10:"+e[i].Syntac+":"+e[e[i].Ref].Syntac+":"+distance] = true
	af["nomXC11:"+e[i].Part+":"+e[e[i].Ref].Part+":"+distance] = true
	af["nomXC12:"+e[i].Word+":"+e[ref].Word+":"+distance] = true
	af["nomXC13:"+e[i].POS+":"+e[ref].POS+":"+distance] = true
	af["nomXC14:"+e[i].Pat+":"+e[ref].Pat+":"+distance] = true
}

func FeatureEvalX(anaType string, feature string, ent map[int]Entity, eid int, candi int) float64 {
	f := strings.Split(feature, ":")
	distance := 0
	distanceX := 0
	ref := 0
	distanceX = ent[eid].Line - ent[candi].Line
	ref = FindChainAna(ent, candi)

	switch len(f) {
	case 3:
		dis, _ := strconv.Atoi(f[2])
		distance = dis
	case 4:
		dis, _ := strconv.Atoi(f[3])
		distance = dis
	}

	switch {
	case anaType == "Zero" && f[0] == "zeroXA1" && f[1] == ent[eid].VerbWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA2" && f[1] == ent[eid].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA3" && f[1] == ent[eid].VerbPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA4" && f[1] == ent[eid].PreWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA5" && f[1] == ent[eid].PrePOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA6" && f[1] == ent[eid].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA7" && f[1] == ent[eid].PostWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA8" && f[1] == ent[eid].PostPOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA9" && f[1] == ent[eid].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA10" && f[1] == ent[eid].Syntac && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA11" && f[1] == ent[eid].Part && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA12" && f[1] == ent[eid].Word && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA13" && f[1] == ent[eid].POS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA14" && f[1] == ent[eid].Pat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA15" && ent[eid].NewPara && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXA16" && ent[eid].NewPara2 && distance == distanceX:
		return 1

	case anaType == "Zero" && f[0] == "zeroXB1" && f[1] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB2" && f[1] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB3" && f[1] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB4" && f[1] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB5" && f[1] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB6" && f[1] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB7" && f[1] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB8" && f[1] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB9" && f[1] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB10" && f[1] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB11" && f[1] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB12" && f[1] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB13" && f[1] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXB14" && f[1] == ent[ref].Pat && distance == distanceX:
		return 1

	case anaType == "Zero" && f[0] == "zeroXC1" && f[1] == ent[eid].VerbWord && f[2] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC2" && f[1] == ent[eid].VerbPOS && f[2] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC3" && f[1] == ent[eid].VerbPat && f[2] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC4" && f[1] == ent[eid].PreWord && f[2] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC5" && f[1] == ent[eid].PrePOS && f[2] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC6" && f[1] == ent[eid].PreWordPat && f[2] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC7" && f[1] == ent[eid].PostWord && f[2] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC8" && f[1] == ent[eid].PostPOS && f[2] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC9" && f[1] == ent[eid].PostWordPat && f[2] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC10" && f[1] == ent[eid].Syntac && f[2] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC11" && f[1] == ent[eid].Part && f[2] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC12" && f[1] == ent[eid].Word && f[2] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC13" && f[1] == ent[eid].POS && f[2] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Zero" && f[0] == "zeroXC14" && f[1] == ent[eid].Pat && f[2] == ent[ref].Pat && distance == distanceX:
		return 1

	case anaType == "Pro" && f[0] == "proXA1" && f[1] == ent[eid].VerbWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA2" && f[1] == ent[eid].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA3" && f[1] == ent[eid].VerbPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA4" && f[1] == ent[eid].PreWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA5" && f[1] == ent[eid].PrePOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA6" && f[1] == ent[eid].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA7" && f[1] == ent[eid].PostWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA8" && f[1] == ent[eid].PostPOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA9" && f[1] == ent[eid].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA10" && f[1] == ent[eid].Syntac && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA11" && f[1] == ent[eid].Part && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA12" && f[1] == ent[eid].Word && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA13" && f[1] == ent[eid].POS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA14" && f[1] == ent[eid].Pat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA15" && ent[eid].NewPara && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXA16" && ent[eid].NewPara2 && distance == distanceX:
		return 1

	case anaType == "Pro" && f[0] == "proXB1" && f[1] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB2" && f[1] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB3" && f[1] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB4" && f[1] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB5" && f[1] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB6" && f[1] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB7" && f[1] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB8" && f[1] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB9" && f[1] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB10" && f[1] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB11" && f[1] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB12" && f[1] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB13" && f[1] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB14" && f[1] == ent[ref].Pat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB15" && f[1] == ent[eid].Word && f[2] == ent[ref].Word:
		return 1
	case anaType == "Pro" && f[0] == "proXB16" && HeadWordMatch(ent[eid].Word, ent[ref].Word) && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXB17" && distance == distanceX:
		w1 := strings.Split(ent[eid].Word, "_")
		pos1 := strings.Split(ent[eid].POS, "_")
		w2 := strings.Split(ent[ref].Word, "_")
		pos2 := strings.Split(ent[ref].POS, "_")
		c1 := w1[0] + ":" + pos1[0]
		c2 := w2[0] + ":" + pos2[0]
		if semDB.ISAWord(c1, c2) {
			return 1
		}

	case anaType == "Pro" && f[0] == "proXC1" && f[1] == ent[eid].VerbWord && f[2] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC2" && f[1] == ent[eid].VerbPOS && f[2] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC3" && f[1] == ent[eid].VerbPat && f[2] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC4" && f[1] == ent[eid].PreWord && f[2] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC5" && f[1] == ent[eid].PrePOS && f[2] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC6" && f[1] == ent[eid].PreWordPat && f[2] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC7" && f[1] == ent[eid].PostWord && f[2] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC8" && f[1] == ent[eid].PostPOS && f[2] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC9" && f[1] == ent[eid].PostWordPat && f[2] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC10" && f[1] == ent[eid].Syntac && f[2] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC11" && f[1] == ent[eid].Part && f[2] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC12" && f[1] == ent[eid].Word && f[2] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC13" && f[1] == ent[eid].POS && f[2] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Pro" && f[0] == "proXC14" && f[1] == ent[eid].Pat && f[2] == ent[ref].Pat && distance == distanceX:
		return 1

	case anaType == "Elipsis" && f[0] == "elipXA1" && f[1] == ent[eid].VerbWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA2" && f[1] == ent[eid].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA3" && f[1] == ent[eid].VerbPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA4" && f[1] == ent[eid].PreWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA5" && f[1] == ent[eid].PrePOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA6" && f[1] == ent[eid].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA7" && f[1] == ent[eid].PostWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA8" && f[1] == ent[eid].PostPOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA9" && f[1] == ent[eid].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA10" && f[1] == ent[eid].Syntac && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA11" && f[1] == ent[eid].Part && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA12" && f[1] == ent[eid].Word && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA13" && f[1] == ent[eid].POS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA14" && f[1] == ent[eid].Pat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA15" && ent[eid].NewPara && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXA16" && ent[eid].NewPara2 && distance == distanceX:
		return 1

	case anaType == "Elipsis" && f[0] == "elipXB1" && f[1] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB2" && f[1] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB3" && f[1] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB4" && f[1] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB5" && f[1] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB6" && f[1] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB7" && f[1] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB8" && f[1] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB9" && f[1] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB10" && f[1] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB11" && f[1] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB12" && f[1] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB13" && f[1] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB14" && f[1] == ent[ref].Pat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB15" && f[1] == ent[eid].Word && f[2] == ent[ref].Word:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB16" && HeadWordMatch(ent[eid].Word, ent[ref].Word) && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXB17" && distance == distanceX:
		w1 := strings.Split(ent[eid].Word, "_")
		pos1 := strings.Split(ent[eid].POS, "_")
		w2 := strings.Split(ent[ref].Word, "_")
		pos2 := strings.Split(ent[ref].POS, "_")
		c1 := w1[0] + ":" + pos1[0]
		c2 := w2[0] + ":" + pos2[0]
		if semDB.ISAWord(c1, c2) {
			return 1
		}

	case anaType == "Elipsis" && f[0] == "elipXC1" && f[1] == ent[eid].VerbWord && f[2] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC2" && f[1] == ent[eid].VerbPOS && f[2] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC3" && f[1] == ent[eid].VerbPat && f[2] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC4" && f[1] == ent[eid].PreWord && f[2] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC5" && f[1] == ent[eid].PrePOS && f[2] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC6" && f[1] == ent[eid].PreWordPat && f[2] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC7" && f[1] == ent[eid].PostWord && f[2] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC8" && f[1] == ent[eid].PostPOS && f[2] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC9" && f[1] == ent[eid].PostWordPat && f[2] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC10" && f[1] == ent[eid].Syntac && f[2] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC11" && f[1] == ent[eid].Part && f[2] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC12" && f[1] == ent[eid].Word && f[2] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC13" && f[1] == ent[eid].POS && f[2] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Elipsis" && f[0] == "elipXC14" && f[1] == ent[eid].Pat && f[2] == ent[ref].Pat && distance == distanceX:
		return 1

	case anaType == "Nom" && f[0] == "nomXA1" && f[1] == ent[eid].VerbWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA2" && f[1] == ent[eid].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA3" && f[1] == ent[eid].VerbPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA4" && f[1] == ent[eid].PreWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA5" && f[1] == ent[eid].PrePOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA6" && f[1] == ent[eid].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA7" && f[1] == ent[eid].PostWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA8" && f[1] == ent[eid].PostPOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA9" && f[1] == ent[eid].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA10" && f[1] == ent[eid].Syntac && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA11" && f[1] == ent[eid].Part && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA12" && f[1] == ent[eid].Word && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA13" && f[1] == ent[eid].POS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA14" && f[1] == ent[eid].Pat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA15" && ent[eid].NewPara && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXA16" && ent[eid].NewPara2 && distance == distanceX:
		return 1

	case anaType == "Nom" && f[0] == "nomXB1" && f[1] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB2" && f[1] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB3" && f[1] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB4" && f[1] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB5" && f[1] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB6" && f[1] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB7" && f[1] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB8" && f[1] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB9" && f[1] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB10" && f[1] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB11" && f[1] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB12" && f[1] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB13" && f[1] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB14" && f[1] == ent[ref].Pat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB15" && f[1] == ent[eid].Word && f[2] == ent[ref].Word:
		return 1
	case anaType == "Nom" && f[0] == "nomXB16" && HeadWordMatch(ent[eid].Word, ent[ref].Word) && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXB17" && distance == distanceX:
		w1 := strings.Split(ent[eid].Word, "_")
		pos1 := strings.Split(ent[eid].POS, "_")
		w2 := strings.Split(ent[ref].Word, "_")
		pos2 := strings.Split(ent[ref].POS, "_")
		c1 := w1[0] + ":" + pos1[0]
		c2 := w2[0] + ":" + pos2[0]
		if semDB.ISAWord(c1, c2) {
			return 1
		}

	case anaType == "Nom" && f[0] == "nomXC1" && f[1] == ent[eid].VerbWord && f[2] == ent[candi].VerbWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC2" && f[1] == ent[eid].VerbPOS && f[2] == ent[candi].VerbPOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC3" && f[1] == ent[eid].VerbPat && f[2] == ent[candi].VerbPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC4" && f[1] == ent[eid].PreWord && f[2] == ent[candi].PreWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC5" && f[1] == ent[eid].PrePOS && f[2] == ent[candi].PrePOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC6" && f[1] == ent[eid].PreWordPat && f[2] == ent[candi].PreWordPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC7" && f[1] == ent[eid].PostWord && f[2] == ent[candi].PostWord && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC8" && f[1] == ent[eid].PostPOS && f[2] == ent[candi].PostPOS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC9" && f[1] == ent[eid].PostWordPat && f[2] == ent[candi].PostWordPat && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC10" && f[1] == ent[eid].Syntac && f[2] == ent[candi].Syntac && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC11" && f[1] == ent[eid].Part && f[2] == ent[candi].Part && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC12" && f[1] == ent[eid].Word && f[2] == ent[ref].Word && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC13" && f[1] == ent[eid].POS && f[2] == ent[ref].POS && distance == distanceX:
		return 1
	case anaType == "Nom" && f[0] == "nomXC14" && f[1] == ent[eid].Pat && f[2] == ent[ref].Pat && distance == distanceX:
		return 1
	}
	return 0
}
